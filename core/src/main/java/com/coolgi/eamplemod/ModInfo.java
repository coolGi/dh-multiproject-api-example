package com.coolgi.eamplemod;

public class ModInfo {
    public static final String ID = "examplemod";
    public static final String NAME = "ExampleMod";
    public static final String READABLE_NAME = "Example mod";
    public static final String VERSION = "1.0.0";
}
