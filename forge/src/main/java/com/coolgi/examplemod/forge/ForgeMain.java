package com.coolgi.examplemod.forge;

import com.coolgi.eamplemod.ModInfo;
import com.coolgi.eamplemod.CommonMain;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;

@Mod(ModInfo.ID)
public class ForgeMain {
    public ForgeMain() {
        // Submit our event bus to let architectury register our content on the right time
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::init);
    }

    private void init(final FMLCommonSetupEvent event) {
        CommonMain.init();
    }
}
