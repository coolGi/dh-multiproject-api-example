package DhApi_common.worldGen;

import com.seibel.distanthorizons.api.enums.worldGeneration.EDhApiDistantGeneratorMode;
import com.seibel.distanthorizons.api.interfaces.override.worldGenerator.AbstractDhApiChunkWorldGenerator;
import net.minecraft.world.level.LevelReader;

import net.minecraft.world.level.chunk.ChunkAccess;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @version 2023-6-22
 */
public class ExampleWorldGenerator extends AbstractDhApiChunkWorldGenerator
{
	private static final Logger LOGGER = LogManager.getLogger();
	
	private final LevelReader level;
	
	
	
	public ExampleWorldGenerator(LevelReader level)
	{
		this.level = level;
	}
	
	
	
	@Override 
	public boolean isBusy() { return false; }
	
	@Override 
	public Object[] generateChunk(int chunkX, int chunkZ, EDhApiDistantGeneratorMode eDhApiDistantGeneratorMode)
	{
		ChunkAccess chunk = this.level.getChunk(chunkX, chunkZ);
		return new Object[] { chunk, this.level };
	}
	
	@Override 
	public void preGeneratorTaskStart()
	{
		// do nothing
	}
	
	@Override 
	public void close()
	{
		// do nothing
	}
	
}
