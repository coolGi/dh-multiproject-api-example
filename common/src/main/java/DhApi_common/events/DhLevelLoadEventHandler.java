package DhApi_common.events;

import DhApi_common.worldGen.ExampleWorldGenerator;
import com.seibel.distanthorizons.api.DhApi;
import com.seibel.distanthorizons.api.interfaces.override.worldGenerator.IDhApiWorldGenerator;
import com.seibel.distanthorizons.api.methods.events.abstractEvents.DhApiLevelLoadEvent;
import com.seibel.distanthorizons.api.methods.events.sharedParameterObjects.DhApiEventParam;

import net.minecraft.world.level.LevelReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @version 2023-6-23
 */
public class DhLevelLoadEventHandler extends DhApiLevelLoadEvent
{
	private static final Logger LOGGER = LogManager.getLogger();

	@Override
	public void onLevelLoad(DhApiEventParam<EventParam> event)
	{
		LOGGER.info("DH Level: ["+event.value.levelWrapper.getDimensionType()+"] loaded.");
		
		// Note: whenever you use a wrapper method on a new Minecraft version it is recommended that you
		// call wrapper.getClass() to determine which object the API will return before you try casting it.
		LevelReader level = (LevelReader) event.value.levelWrapper.getWrappedMcObject();
		
		// override the core DH world generator for this level
		IDhApiWorldGenerator exampleWorldGen = new ExampleWorldGenerator(level);
		DhApi.worldGenOverrides.registerWorldGeneratorOverride(event.value.levelWrapper, exampleWorldGen);
	}
	
}
