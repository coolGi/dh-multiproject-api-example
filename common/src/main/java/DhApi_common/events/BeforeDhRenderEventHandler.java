package DhApi_common.events;

import com.seibel.distanthorizons.api.DhApi;
import com.seibel.distanthorizons.api.interfaces.world.IDhApiLevelWrapper;
import com.seibel.distanthorizons.api.methods.events.abstractEvents.DhApiBeforeRenderEvent;
import com.seibel.distanthorizons.api.methods.events.sharedParameterObjects.DhApiCancelableEventParam;
import com.seibel.distanthorizons.api.objects.DhApiResult;
import com.seibel.distanthorizons.api.objects.data.DhApiRaycastResult;
import com.seibel.distanthorizons.api.objects.math.DhApiVec3i;
import net.minecraft.client.Camera;
import net.minecraft.client.Minecraft;
import net.minecraft.client.player.LocalPlayer;
import net.minecraft.network.chat.Component;
import net.minecraft.world.level.block.state.BlockState;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joml.Vector3f;

/**
 * @version 2023-6-23
 */
public class BeforeDhRenderEventHandler extends DhApiBeforeRenderEvent
{
	private static final Logger LOGGER = LogManager.getLogger();
	
	private BlockState previousBlockState = null;
	
	
	
	@Override 
	public void beforeRender(DhApiCancelableEventParam<EventParam> event)
	{
		// wait for a Minecraft level to load

		Minecraft mc = Minecraft.getInstance();
		if (mc == null)
		{
			return;
		}

		LocalPlayer player = mc.player;
		if (player == null)
		{
			return;
		}
		
		if (!DhApi.Delayed.worldProxy.worldLoaded())
		{
			return;
		}
		IDhApiLevelWrapper levelWrapper = DhApi.Delayed.worldProxy.getSinglePlayerLevel();
		if (levelWrapper == null)
		{
			return;
		}
		
		// a minecraft level is loaded
		
		
		// attempt to get the DH datapoint that the player is looking at
		Camera camera = mc.gameRenderer.getMainCamera();
		Vector3f cameraDir = camera.getLookVector();
		DhApiResult<DhApiRaycastResult> rayCastResult = DhApi.Delayed.terrainRepo.raycast(
				levelWrapper,
				camera.getBlockPosition().getX(), camera.getBlockPosition().getY(), camera.getBlockPosition().getZ(),
				cameraDir.x(), cameraDir.y(), cameraDir.z(),
				// how far in blocks to look before giving up
				2000);
		
		
		BlockState newBlockState = null;
		DhApiVec3i rayCastBlockPos = null;
		if (rayCastResult.success && rayCastResult.payload != null)
		{
			// the raycast successfully hit a block
			
			// Note: whenever you use a wrapper method on a new Minecraft version it is recommended that you
			// call wrapper.getClass() to determine which object the API will return before you try casting it.
			newBlockState = (BlockState) rayCastResult.payload.dataPoint.blockStateWrapper.getWrappedMcObject();
			rayCastBlockPos = rayCastResult.payload.pos;
		}
		
		
		// only send a chat message when the block changes to reduce chat/log spam 
		if (this.previousBlockState != newBlockState)
		{
			this.previousBlockState = newBlockState;
			
			String blockName = "NULL";
			if (newBlockState != null)
			{
				blockName = newBlockState.getBlock().getDescriptionId();
			}

			double rayDistance = -1;
			if (rayCastBlockPos != null)
			{
				rayDistance =
						Math.sqrt(
								Math.pow(rayCastBlockPos.x - camera.getBlockPosition().getX(), 2) +
										Math.pow(rayCastBlockPos.y - camera.getBlockPosition().getY(), 2) +
										Math.pow(rayCastBlockPos.z - camera.getBlockPosition().getZ(), 2));
				rayDistance = Math.round(rayDistance * 100);
				rayDistance = rayDistance / 100.0;
			}
			
			String message = "block: ["+blockName+"] pos: ["+rayCastBlockPos+"] distance: ["+rayDistance+"]"; 
			player.displayClientMessage(Component.literal(message), false);
			//LOGGER.info(message);
		}
	}
	
}
