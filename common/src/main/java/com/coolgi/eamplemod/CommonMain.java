package com.coolgi.eamplemod;

import DhApi_common.events.BeforeDhRenderEventHandler;
import DhApi_common.events.DhLevelLoadEventHandler;
import DhApi_core.events.AfterDhInitEventHandler;
import com.seibel.distanthorizons.api.DhApi;
import com.seibel.distanthorizons.api.methods.events.DhApiEventRegister;
import com.seibel.distanthorizons.api.methods.events.abstractEvents.DhApiAfterDhInitEvent;
import com.seibel.distanthorizons.api.methods.events.abstractEvents.DhApiBeforeRenderEvent;
import com.seibel.distanthorizons.api.methods.events.abstractEvents.DhApiLevelLoadEvent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CommonMain {
    public static final Logger LOGGER = LogManager.getLogger("examplemod");

    public static void init() {
        System.out.println("Inited + " + ModInfo.READABLE_NAME);
        manifoldTest();

        dhInit();
    }

    public static void dhInit() {
        // This code runs as soon as Minecraft is in a mod-load-ready state.
        // However, some things (like resources) may still be uninitialized.

        // This means some DH api methods may not be fully set up yet.

        // DhApi and its immediate methods are always available to use
        Class<?> alwaysAvailable = DhApi.class;
        // and everything in DhApi.Delayed needs DH to fully load before it can be used.
        Class<?> requiresDhToFullyLoad = DhApi.Delayed.class;




        // API versioning //
        LOGGER.info("Attempting to use the Distant Horizons API...");

        // DH Version
        String dhVersion = DhApi.getModVersion();
        LOGGER.info("DH version: " + dhVersion);
        // API version
        int dhApiMajorVersion = DhApi.getApiMajorVersion();
        int dhApiMinorVersion = DhApi.getApiMinorVersion();
        LOGGER.info("DH API version: " + dhApiMajorVersion + "." + dhApiMinorVersion);



        // event Setup //
        LOGGER.info("Registering API events...");

        // terrain data repo event setup //
        DhApiBeforeRenderEvent beforeRenderEvent = new BeforeDhRenderEventHandler();
        DhApiEventRegister.on(DhApiBeforeRenderEvent.class, beforeRenderEvent);


        // accessing a delayed dH method at your mod's startup ///
        try
        {
            // if DH hasn't been initialized yet this will throw an exception
            DhApi.Delayed.configs.graphics().renderingEnabled().getValue();
            LOGGER.info("DH initialization has been completed.");
        }
        catch (Exception e)
        {
            LOGGER.info("Caught expected exception. DH initialization hasn't been completed.");
        }
        // Instead of depending on a specific mod load order, use the following event instead.
        // This event will fire either after DH finishes loading,
        // or immediately if DH is already loaded.
        DhApiEventRegister.on(DhApiAfterDhInitEvent.class, new AfterDhInitEventHandler());


        // world generator example setup //
        DhApiEventRegister.on(DhApiLevelLoadEvent.class, new DhLevelLoadEventHandler());


        LOGGER.info("Finished Registering DH Events.");
    }

    public static void manifoldTest() {
        // Quick test for Manifold (should only compile one of these)
        #if MC_1_16_5
        System.out.println("Manifold Preprocessor test..., running 1.16.5");
        #elif MC_1_17_1
        System.out.println("Manifold Preprocessor test..., running 1.17.1");
        #elif MC_1_18_2
        System.out.println("Manifold Preprocessor test..., running 1.18.2");
        #elif MC_1_19_2
        System.out.println("Manifold Preprocessor test..., running 1.19.2");
        #elif MC_1_19_4
        System.out.println("Manifold Preprocessor test..., running 1.19.4");
        #elif MC_1_20_1
        System.out.println("Manifold Preprocessor test..., running 1.20.1");
        #elif MC_1_20_2
        System.out.println("Manifold Preprocessor test..., running 1.20.2");
        #endif
    }
}
